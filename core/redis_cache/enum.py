from enum import Enum


class RedisCacheKey(str, Enum):
    cities = "cities:by:user"


class RedisExpiration(int, Enum):
    exp_time = 60 * 60  # 1 hour
