from core.connection.redis_connection import redis_conn
from core.redis_cache.enum import RedisCacheKey, RedisExpiration


async def save_city(user_id: int, city: str) -> None:
    _redis = await redis_conn.redis
    key = f"{RedisCacheKey.cities.value}:{user_id}"
    cities = await _redis.lrange(key, 0, -1)
    if city not in cities:
        await _redis.lpush(key, city)
        await _redis.ltrim(key, 0, 4)
        await _redis.expire(key, RedisExpiration.exp_time.value)


async def get_last_cities(user_id: int) -> list | None:
    _redis = await redis_conn.redis
    key = f"{RedisCacheKey.cities.value}:{user_id}"
    cities = await _redis.lrange(key, 0, -1)
    return cities
