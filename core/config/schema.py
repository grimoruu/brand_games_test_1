from pydantic import BaseModel


class RedisSettings(BaseModel):
    host: str
    port: int

    def redis_url(self) -> str:
        return f"redis://{self.host}:{self.port}"


class TgTokenSettings(BaseModel):
    token: str


class WeatherApiSettings(BaseModel):
    key: str


class Settings(BaseModel):
    redis: RedisSettings
    tg_token: TgTokenSettings
    weather_api_key: WeatherApiSettings
