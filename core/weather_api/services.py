import logging
from datetime import datetime
from math import ceil

import aiohttp

from core.config.settings import settings
from core.weather_api.codes import code_to_smile


async def weather_service_api(city: str | None) -> dict | None:
    api_key = settings.weather_api_key.key
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&lang=ru&units=metric&appid={api_key}"

    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                data = await response.json()

                city = data["name"]
                cur_temp = data["main"]["temp"]
                humidity = data["main"]["humidity"]
                pressure = data["main"]["pressure"]
                wind = data["wind"]["speed"]
                sunrise_timestamp = datetime.fromtimestamp(data["sys"]["sunrise"])
                sunset_timestamp = datetime.fromtimestamp(data["sys"]["sunset"])
                length_of_the_day = sunset_timestamp - sunrise_timestamp
                weather_description = data["weather"][0]["main"]
                wd = code_to_smile.get(weather_description, "Непонятно все это(")

                weather_data = {
                    "city": city,
                    "temperature": cur_temp,
                    "humidity": humidity,
                    "pressure": ceil(pressure / 1.333),  # Преобразование в мм рт. ст.
                    "wind_speed": wind,
                    "sunrise": sunrise_timestamp,
                    "sunset": sunset_timestamp,
                    "length_of_day": length_of_the_day,
                    "wd": wd,
                }

                return weather_data
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return None


def weather_message(weather_data: dict) -> str:
    weather = (
        f"Текущая погода в городе {weather_data['city']}:\n"
        f"Температура: {weather_data['temperature']}°C {weather_data['wd']}\n"
        f"Влажность: {weather_data['humidity']}%\n"
        f"Давление: {weather_data['pressure']} мм.рт.ст\n"
        f"Скорость ветра: {weather_data['wind_speed']} м/с\n"
        f"Восход солнца: {weather_data['sunrise'].strftime('%Y-%m-%d %H:%M')}\n"
        f"Закат солнца: {weather_data['sunset'].strftime('%Y-%m-%d %H:%M')}\n"
        f"Продолжительность дня: {weather_data['length_of_day']}\n"
        f"Хорошего дня!"
    )
    return weather
