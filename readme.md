# WishBot
Бот предназначен для того, чтобы помочь вам получить актуальную информацию о погоде в различных городах. Независимо от того, где вы находитесь, вы можете быстро узнать текущие погодные условия или просмотреть предыдущие запросы о погоде.
## Установка и Запуск

1. Клонирование репозитория:

```bash
git clone https://gitlab.com/grimoruu/brand_games_test_1.git
```
2. Директория проекта:
```bash
cd brand_games_test_1/
```
3. Установка зависимостей poetry:
```bash
poetry install
```
4. Сборка и запуск докеров:
```bash
docker compose build
```
```bash
docker compose up -d
```
5. Запуск бота
```bash
python3 main.py
```
Бот будет доступен по адресу tg @brand_games_weather_bot
