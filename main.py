import asyncio
import logging
import sys

from bot.handlers import bot, dp
from core.connection.redis_connection import redis_conn


async def main() -> None:
    try:
        await redis_conn.connect()
        await dp.start_polling(bot)
    finally:
        await redis_conn.disconnect()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
