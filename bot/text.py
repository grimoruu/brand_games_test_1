class TextMessages:
    PAST_REQUESTS_PROMPT = "У вас уже есть предыдущие запросы погоды. Что вы хотели бы сделать?"
    WELCOME_MESSAGE = "Добро пожаловать! Что вы хотели бы сделать?"
    CITY_WEATHER_PROMPT = "Введите название города, чтобы узнать погоду:"
    EMPTY_CITY_LIST_MESSAGE = "У вас пока нет сохраненных городов для просмотра погоды."
    USE_MENU_BUTTONS_MESSAGE = "Пожалуйста, используйте кнопки меню для выбора функций."
    CHECK_CITY_NAME_MESSAGE = "Проверьте пожалуйста название города!"


class TextKeyboards:
    RETURN_TO_MAIN_MENU = "Вернуться в главное меню"
    VIEW_PREVIOUS_WEATHER_REQUESTS = "Посмотреть предыдущие запросы погоды"
    GET_WEATHER_SUMMARY = "Получить сводку погоды"
