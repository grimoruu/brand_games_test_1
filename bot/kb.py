from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from bot.text import TextKeyboards


class Kb:
    menu_kb = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text=TextKeyboards.RETURN_TO_MAIN_MENU, callback_data="return_to_menu")],
            [InlineKeyboardButton(text=TextKeyboards.VIEW_PREVIOUS_WEATHER_REQUESTS, callback_data="get_weathers")],
        ]
    )

    start_menu_kb = InlineKeyboardMarkup(
        inline_keyboard=[
            [InlineKeyboardButton(text=TextKeyboards.GET_WEATHER_SUMMARY, callback_data="get_weather")],
        ]
    )

    return_to_menu_kb = InlineKeyboardMarkup(
        inline_keyboard=[[InlineKeyboardButton(text=TextKeyboards.RETURN_TO_MAIN_MENU, callback_data="return_to_menu")]]
    )
