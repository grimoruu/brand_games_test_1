from aiogram import Bot, Dispatcher, types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext

from bot.kb import Kb
from bot.states import MenuState
from bot.text import TextMessages
from core.config.settings import settings
from core.redis_cache.services import get_last_cities, save_city
from core.weather_api.services import weather_message, weather_service_api

bot = Bot(token=settings.tg_token.token)
dp = Dispatcher()


@dp.message(Command("start"))
async def cmd_start(message: types.Message, state: FSMContext) -> None:
    await state.set_state(MenuState.IN_MENU)
    user_id = message.from_user.id if message.from_user else None
    cities = await get_last_cities(user_id) if user_id else None
    if cities:
        await message.answer(TextMessages.PAST_REQUESTS_PROMPT, reply_markup=Kb.menu_kb)
    else:
        await message.answer(TextMessages.WELCOME_MESSAGE, reply_markup=Kb.start_menu_kb)


@dp.callback_query(lambda callback_query: callback_query.data == "get_weather")
async def get_weather(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    if callback_query.message:
        await bot.send_message(callback_query.message.chat.id, TextMessages.CITY_WEATHER_PROMPT)
    await state.set_state(None)


@dp.callback_query(lambda callback_query: callback_query.data == "get_weathers")
async def get_weathers(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    user_id = callback_query.from_user.id
    cities = await get_last_cities(user_id)
    if cities:
        for city in cities:
            weather_data = await weather_service_api(city)
            if weather_data:
                weather = weather_message(weather_data)
                if city == cities[-1]:

                    await callback_query.message.answer(weather, reply_markup=Kb.return_to_menu_kb)
                else:
                    await callback_query.message.answer(weather)
    else:
        await bot.send_message(callback_query.message.chat.id, TextMessages.EMPTY_CITY_LIST_MESSAGE)
    await state.set_state(None)


@dp.message(MenuState.IN_MENU)
async def handle_menu_messages(message: types.Message, state: FSMContext) -> None:
    await message.reply(TextMessages.USE_MENU_BUTTONS_MESSAGE)
    await cmd_start(message, state)


@dp.message()
async def handle_text_message(message: types.Message) -> None:
    city = message.text
    if message.from_user:
        user_id = message.from_user.id
    else:
        user_id = None
    weather_data = await weather_service_api(city)
    if weather_data and city and user_id:
        await save_city(user_id, city)
        weather = weather_message(weather_data)
        await message.reply(weather, reply_markup=Kb.menu_kb)
    else:
        await message.reply(TextMessages.CHECK_CITY_NAME_MESSAGE)


@dp.callback_query(lambda callback_query: callback_query.data == "return_to_menu")
async def return_to_menu(callback_query: types.CallbackQuery, state: FSMContext) -> None:
    await cmd_start(callback_query.message, state)
